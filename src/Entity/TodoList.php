<?php
// api/src/Entity/TodoList.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource
 * @ORM\Entity
 */
class TodoList
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string The name of this TodoList
     *
     * @ORM\Column
     */
    public $name;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     */
    public $creationDate;

    /**
     * @var TodoListEntry[] Todo list entries
     *
     * @ORM\OneToMany(targetEntity="TodoListEntry", mappedBy="owningList", cascade={"persist", "remove"})
     */
    public $items;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }
}
