<?php
// api/src/Entity/TodoListEntry.php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource
 * @ORM\Entity
 */
class TodoListEntry
{


    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    public $body;

    /**
     * @var \DateTimeInterface The date this TodoListEntry was submitted
     *
     * @ORM\Column(type="datetime")
     */
    public $creationDate;

    /**
     * @var TodoList
     *
     * @ORM\ManyToOne(targetEntity="TodoList", inversedBy="items")
     */
    public $owningList;
}
